﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour {

	public			GameObject 			trapeze;
	private			ActionController actionControler;
	private			SceneStage			mainScene;
	private			MainCamera			mainCamera;

	public	 		int					STATE;
	public	 		string	 			pState;

	public	 		bool				stillInCicleTrapeze;	

	public	 		int					playerDir;

	void Start () {
		actionControler = new ActionController();

		mainScene = GameObject.Find ("SceneStage").GetComponent<SceneStage> ();

		mainCamera = Camera.main.GetComponent<MainCamera> ();
		ChangeState (Define.STATE_CHAR_BEGIN);
	}

	void Update () {
		actionControler.Update ();

		switch (STATE) {
		case Define.STATE_CHAR_ROTATING_ROPE:
		case Define.STATE_CHAR_BEGIN:
			if(trapeze.name == "Trapeze")
				transform.eulerAngles = trapeze.transform.eulerAngles;
			break;		
		}
		
		if (!actionControler.allActionsActive ()) {
			switch(STATE){
			case Define.STATE_CHAR_IN_CANON:
				if(Input.GetMouseButtonDown(0))
					ChangeState(Define.STATE_CHAR_FIRE_BY_CANON);

				foreach (Touch touch in Input.touches) {						
					if (touch.phase == TouchPhase.Began){
						ChangeState(Define.STATE_CHAR_FIRE_BY_CANON);
					}					
				}
				break;
			}
		}
	}

	void RestartCombo() {
		curentCoin = 0;
		maxNumberCoin = 0;
	}

	void makeDirectionJump(int dir) {
		//1 is left, 2 is right
		playerDir = dir;
	}


	#region ChangeState
	public void ChangeState(int state){
		actionControler.stopAllAction ();

		switch (state) {

		case Define.STATE_CHAR_BEGIN:

			trapeze = GameObject.FindGameObjectWithTag("TrapezeBegin");
			trapeze.GetComponent<Trapeze>().AddPlayer();
			mainScene.AddTrapeze(trapeze);
			
			actionControler.add(new Player_Action_Begin(gameObject, trapeze));

			ChangeAnimation(Define.ANIM_IDLE);
			pState = "STATE_CHAR_BEGIN";

			break;

		case Define.STATE_CHAR_JUMP:

			actionControler.add(new Player_Action_Jump(gameObject));

			transform.GetComponent<BoxCollider2D>().size = new Vector2(6,6.46f);

			ChangeAnimation(Define.ANIM_JUMP);

			pState = "STATE_CHAR_JUMP";
			
			break;

		case Define.STATE_CHAR_ROTATING_ROPE:
			rigidbody2D.isKinematic = true;

			transform.GetComponent<BoxCollider2D>().size = new Vector2(6,11.6f);

			ChangeAnimation(Define.ANIM_IDLE);

			actionControler.add(new Player_Action_Rotating(gameObject, trapeze));

			RestartCombo();

			pState = "STATE_CHAR_ROTATING_ROPE";
			
			break;		

		case Define.STATE_CHAR_ROTATING_BALL:
			rigidbody2D.isKinematic = true;			

			transform.GetComponent<BoxCollider2D>().size = new Vector2(6,11.6f);


			ChangeAnimation(Define.ANIM_ROTATING_BALL);			
			
			actionControler.add(new Player_Action_Rotating(gameObject, trapeze));
			
			RestartCombo();
			
			pState = "STATE_CHAR_ROTATING_BALL";
			
			break;	

		case Define.STATE_CHAR_IN_CANON:
			rigidbody2D.isKinematic = true;

			ChangeAnimation(Define.ANIM_IDLE);

			actionControler.add(new Player_Action_In_Canon(gameObject, trapeze));
			
			pState = "STATE_CHAR_IN_CANON";

			RestartCombo();

			break;

		case Define.STATE_CHAR_FIRE_BY_CANON:
			actionControler.add(new Player_Action_Fire_By_Canon(gameObject));
			
			ChangeAnimation(Define.ANIM_JUMP);
			
			pState = "STATE_CHAR_FIRE_BY_CANON";
			
			break;

		case Define.STATE_CHAR_FALL:
			rigidbody2D.isKinematic = false;

			pState = "STATE_CHAR_FALL";
			
			break;
		}


		STATE = state;		
	}

	public void ChangeAnimation(int anim){
		switch (anim) {
			
		case Define.ANIM_IDLE:

			animation.Play("Anim_Idle");
			break;

		case Define.ANIM_JUMP:

			animation.Play("Anim_Jump");
			break;

		case Define.ANIM_ROTATING:

			animation.Play("Anim_Rotate");		
			break;

		case Define.ANIM_ROTATING_BALL:
			animation.Play("Anim_Rotate_Ball");		
			break;
		}
	}


	#endregion

	#region Colision

	void OnTriggerEnter2D(Collider2D coli) {
		if (coli.name == "Trapeze") {
			trapeze = coli.gameObject;
			
			mainScene.AddTrapeze (trapeze);

			mainCamera.StartUpdatePosition();

			stillInCicleTrapeze = true;

		} else if (coli.tag == "Ballon") {
			trapeze = coli.gameObject;

			mainScene.AddTrapeze (trapeze);

			mainCamera.StartUpdatePosition();

			stillInCicleTrapeze = true;
		} else if (coli.tag == "Canon" && STATE != Define.STATE_CHAR_FIRE_BY_CANON) {

			trapeze = coli.gameObject;

			ChangeState(Define.STATE_CHAR_IN_CANON);

		} else if (coli.name == "Coin") {
			if(coli.GetComponent<Coin>().isFirstCombo){
				maxNumberCoin = coli.GetComponent<Coin>().maxNumberCoin;
			}

			curentCoin++;

			if(curentCoin == maxNumberCoin && maxNumberCoin != 0){
				currentCombo++;

				mainScene.ShowCombo();
			}

			coin+=10;
			Destroy(coli.gameObject);
		} 


	}

	public	 		int					maxNumberCoin;
	public	 		int					curentCoin;
	public	 		int					currentCombo;

	public	 		int					coin;
	public	 		int					number;
	
	
//	void OnTriggerStay2D(Collider2D coli) {			print ("trig");
//
//		if (coli.tag == "Trapeze" || coli.tag == "Ballon") {
//
//			stillInCicleTrapeze = true;
//			
//		}
//	}

	void OnTriggerExit2D(Collider2D coli) {

		stillInCicleTrapeze = false;

		mainScene.RemoveTrapeze ();
	}
	#endregion
}
