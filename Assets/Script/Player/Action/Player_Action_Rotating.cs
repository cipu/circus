using UnityEngine;
using System.Collections;

public class Player_Action_Rotating : IActionPhuc {

	GameObject trapeze;

	float 			frequency = 0.45f;
	float 			angle;
	float 			cos;
	float 			currentMaxAngle = 90;

	GameObject		playerHand;
	public Player_Action_Rotating (GameObject player, GameObject trapeze){
		this.player = player;
		this.trapeze = trapeze;
	}

	public override void Update () {
		if(Application.platform == RuntimePlatform.Android){
			ActionTouch ();
		}else if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer){
			ActionKey();
		}
	}	
	
	void ActionKey(){
		if (Input.GetMouseButton (0)) {
			if(trapeze.tag == "Trapeze"){
				foreach(Transform child in trapeze.transform)
				{
					if(child.name == "PlayerPosition")
					{
						player.transform.position = child.position;
						break;
					}
				}
			}else{
//				// Get the value of cos range from -1 to 1
//				float angleSpeed = frequency * Mathf.PI * 2;
//				angle += angleSpeed * Time.deltaTime;
//				cos = Mathf.Cos (angle);
//				
//				// Rotate the trapeze base on the cos value
//				Vector3 euler = player.transform.localEulerAngles;
//				euler.z = cos * currentMaxAngle;
//				player.transform.localEulerAngles = euler;
//				
				foreach(Transform child in trapeze.transform)
				{
					if(child.name == "PlayerPosition")
					{
						player.transform.position = child.position;
						break;
					}
				}
			}
		}
	}
	
	void ActionTouch(){
		foreach (Touch touch in Input.touches) {			
			if(touch.phase == TouchPhase.Stationary){	
				if(trapeze.tag == "Trapeze"){
					foreach(Transform child in trapeze.transform)
					{
						if(child.name == "PlayerPosition")
						{
							player.transform.position = child.position;
							break;
						}
					}
				}else{
//					// Get the value of cos range from -1 to 1
//					float angleSpeed = frequency * Mathf.PI * 2;
//					angle += angleSpeed * Time.deltaTime;
//					cos = Mathf.Cos (angle);
//					
//					// Rotate the trapeze base on the cos value
//					Vector3 euler = player.transform.localEulerAngles;
//					euler.z = cos * currentMaxAngle;
//					player.transform.localEulerAngles = euler;
					
					foreach(Transform child in trapeze.transform)
					{
						if(child.name == "PlayerPosition")
						{
							player.transform.position = child.position;
							break;
						}
					}
				}
			}
		}
	}
	

}
