using UnityEngine;
using System.Collections;

public class Player_Action_Begin : IActionPhuc {

	GameObject trapeze;
	public Player_Action_Begin (GameObject player, GameObject trapeze){
		this.player = player;
		this.trapeze = trapeze;
	}

	public override void Update () {

		foreach(Transform child in trapeze.transform)
		{
			if(child.name == "PlayerPosition")
			{
				player.transform.position = child.position;
				break;
			}
		}

	}
}
