using UnityEngine;
using System.Collections;

public class Player_Action_Fire_By_Canon : IActionPhuc {

	float 			power = 1000.0f;
	float 			whichDirection;

	public Player_Action_Fire_By_Canon (GameObject player){
		this.player = player;

		if(Application.platform == RuntimePlatform.Android){
			power = 600.0f;
		}else if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer){
			power = 1000.0f;
		}	

		whichDirection = player.GetComponent<Player> ().trapeze.GetComponent<Canon> ().whichDirectionFace;

		player.transform.rigidbody2D.isKinematic =  false; 
		player.transform.rigidbody2D.AddForce(GetForce(Input.mousePosition));

		player.transform.eulerAngles = Vector3.zero;
	}

	Vector2 GetForce(Vector3 mouse){
		Vector2 v1 = new Vector2 (player.transform.position.x, player.transform.position.y);
		
		Vector2 v2 = GetVector(whichDirection);		

		Vector2 vSum = (v1 + v2);

//		print("v1: " + v1 +" + "+ "v2: " + v2 + " v1 - v2 = " + vSum.normalized);

		return vSum.normalized * power;		
	}
	
	Vector2 GetVector(float angle){
		float a = -(player.transform.position.x) + Mathf.Cos(angle* Mathf.Deg2Rad) * 10;
		float b = -(player.transform.position.y) + Mathf.Sin (angle* Mathf.Deg2Rad) * 10;
		
		return new Vector2 (a, b);
	}
}
