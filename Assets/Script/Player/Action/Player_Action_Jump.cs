using UnityEngine;
using System.Collections;

public class Player_Action_Jump : IActionPhuc {
	float 			power = 700.0f;

	Player			playerComponent;
	public Player_Action_Jump (GameObject player){
		this.player = player;	

		playerComponent = player.GetComponent<Player> ();

		if (playerComponent.STATE == Define.STATE_CHAR_ROTATING_BALL) {
			power = 450;

			for (int i = 0; i < 4; i++) {
				player.transform.GetChild(i).transform.localScale = new Vector3(1,1,1);
			}
		}

			
		player.transform.rigidbody2D.isKinematic =  false; 
		player.transform.rigidbody2D.AddForce(GetForce(Input.mousePosition));   
	}

	public override void Update () {
		     
	}	

	Vector2 GetForce(Vector3 mouse){
		Vector2 v1 = new Vector2 (player.transform.position.x, player.transform.position.y);

		Vector2 v2 = new Vector2(0,0);
		
		if (playerComponent.STATE == Define.STATE_CHAR_ROTATING_BALL) { 
			if (playerComponent.playerDir == Define.DIR_LEFT) {
				v2 = GetVector (135);
				
				player.transform.localScale = new Vector3 (Mathf.Abs(player.transform.localScale.x) * -1, player.transform.localScale.y, player.transform.localScale.z);
			} else if (playerComponent.playerDir == Define.DIR_RIGHT) {
				v2 = GetVector (45);
			}
		} else {
			v2 = GetVector (player.transform.eulerAngles.z - 90);	

			if(player.transform.eulerAngles.z <= 180)
				player.transform.localScale = new Vector3 (Mathf.Abs(player.transform.localScale.x), player.transform.localScale.y, player.transform.localScale.z);
			else
				player.transform.localScale = new Vector3 (Mathf.Abs(player.transform.localScale.x) * -1, player.transform.localScale.y, player.transform.localScale.z);
		}


		Vector2 vSum = (v1 + v2);

//		print("v1: " + v1 +" + "+ "v2: " + v2 + " v1 - v2 = " + vSum.normalized);

		return vSum.normalized * power;
		
	}

	Vector2 GetVector(float angle){
		float a = -(player.transform.position.x) + Mathf.Cos(angle* Mathf.Deg2Rad) * 10;
		float b = -(player.transform.position.y) + Mathf.Sin (angle* Mathf.Deg2Rad) * 10;
		
		return new Vector2 (a, b);
	}
}
