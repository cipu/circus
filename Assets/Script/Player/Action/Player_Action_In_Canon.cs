using UnityEngine;
using System.Collections;

public class Player_Action_In_Canon : IActionPhuc {

	private 	 		Canon				canon;

	private				float				curTime;
	public Player_Action_In_Canon (GameObject player, GameObject canon){
		this.player = player;

		this.canon = canon.GetComponentInChildren<Canon> ();

		this.canon.enabled = true;

		player.transform.eulerAngles = new Vector3(0,0, this.canon.whichDirectionFace+270);

		if(this.canon.whichDirectionFace == 45)
			player.transform.position = this.canon.transform.position + new Vector3(2f,2f,0);
		else if(this.canon.whichDirectionFace == 90)
			player.transform.position = this.canon.transform.position + new Vector3(-0.3f,3.0f,0);
		else if(this.canon.whichDirectionFace == 270)
			player.transform.position = this.canon.transform.position + new Vector3(0.3f,-3.0f,0);
		else if(this.canon.whichDirectionFace == 315)
			player.transform.position = this.canon.transform.position + new Vector3(2.5f,-1.8f,0);

		curTime = Time.time;
	}

	public override void Update () {
		if(Time.time - curTime >= 1)
			actionDone = true;
//		if (player.transform.rotation.eulerAngles.z >= 315 || player.transform.rotation.eulerAngles.z == 0) {
//			player.transform.Rotate (Vector3.back, 45 * Time.deltaTime);
//
//			player.transform.position = this.canon.transform.position + new Vector3(1,2,0);
//		} else {
//			actionDone = true;
//		}
	}	
}
