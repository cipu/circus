using UnityEngine;
using System.Collections;

public class Trapeze : MonoBehaviour {
	// Frequency of trapeze
	float 			angle;

	bool			trapezeIsRight;
	bool 			isTouching;
	bool 			canRotate;

	bool 			havePlayer;

	float 			cos;

	void Start () {
	}
	
	void Update () {
		float angleSpeed = 300;

		if (isTouching) {

			angle += angleSpeed * Time.deltaTime;			

			Vector3 euler = transform.localEulerAngles;
			euler.z = angle;
			if (euler.z >= 360) {
				euler.z = 0;
				angle = 0;
			}
			transform.localEulerAngles = euler;			
		} else if (!isTouching && canRotate) {
			angleSpeed = 200;

			if(!trapezeIsRight){
				angle += angleSpeed * Time.deltaTime;

				Vector3 euler = transform.localEulerAngles;
				euler.z = angle;

				if (euler.z >= 360) {
					euler.z = 0;
					angle = 0;

					canRotate = false;
				}
				transform.localEulerAngles = euler;	
			}else if(trapezeIsRight){
				angle -= angleSpeed * Time.deltaTime;
				
				Vector3 euler = transform.localEulerAngles;
				euler.z = angle;
				
				if (euler.z <= 0) {
					euler.z = 0;
					angle = 0;
					
					canRotate = false;
				}
				transform.localEulerAngles = euler;	
			}
		}
	}

	public void StartTouchPlayer()
	{
		if (havePlayer) {
			isTouching = true;
		}
	}

	public void StopTouchPlayer()
	{
		isTouching = false;

		if (transform.localEulerAngles.z > 180) {
			trapezeIsRight = false;
		} else if (transform.localEulerAngles.z < 180) {
			trapezeIsRight = true;
		}
		
		canRotate = true;
//		currentMaxAngle = 360 - transform.localEulerAngles.z;
	}

	public void AddPlayer(){
		havePlayer = true;
	}

	public void RemovePlayer(){
		havePlayer = false;
	}

}
