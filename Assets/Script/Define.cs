﻿using UnityEngine;
using System.Collections;


public class Define {

	public static float WIDTH = 800;
	public static float HEIGHT = 480;

	
	public static int ACTION_KIND_SEQUENCE = 0;
	public static int ACTION_KIND_SPAWN = 1;

	public const int DIR_LEFT = 1;
	public const int DIR_RIGHT = 2;

	public const int STATE_CHAR_BEGIN = 0;
	public const int STATE_CHAR_JUMP = 1;
	public const int STATE_CHAR_ROTATING_ROPE = 2;
	public const int STATE_CHAR_ROTATING_BALL = 3;
	public const int STATE_CHAR_IN_CANON = 4;
	public const int STATE_CHAR_FIRE_BY_CANON = 5;
	public const int STATE_CHAR_FALL = 6;

	public const int ANIM_IDLE = 0;
	public const int ANIM_JUMP = 1;	
	public const int ANIM_ROTATING = 2;
	public const int ANIM_ROTATING_BALL = 3;

	public const int STATE_SCENE_RUN = 0;
	public const int STATE_SCENE_PAUSE = 1;


}
