using UnityEngine;
using System.Collections;

public class SceneStage : MonoBehaviour {
	private 		Player 				player;
	private 		GameObject			trapeze;

	private 		StageController		stageController;

	public	 		int					STATE;


	private			Texture				icon_Coin;
	private	 		Button3 			b_Key_Pause;

	private	 		int					coin;
	private	 		bool				showCombo;
	private	 		float				delayTimeShowCombo;

	public GUISkin mygui;
	void Awake(){
		player = GameObject.FindWithTag ("Player").GetComponent<Player> ();

		Screen.SetResolution (800, 480, true);
	}
	void Start () {
		stageController = new StageController ();

		b_Key_Pause = new Button3 ((Texture2D)Resources.Load ("StagePause/PauseButton"), new Rect(Screen.width - 50, 0, 50,50));

		icon_Coin = (Texture)Resources.Load ("Coin");
	}	

	void Update () {
		if(Application.platform == RuntimePlatform.Android){
			ActionTouch ();
		}else if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer){
			ActionKey();
		}		
		
		stageController.Update ();

		UpdateCoin ();
	}

	#region Action
	void ActionKey(){
		if (trapeze != null && player.STATE != Define.STATE_CHAR_FALL) {
			
			if (Input.GetMouseButton (0) || Input.GetKey("a")) {
				if (!b_Key_Pause.rect.Contains (new Vector2 (Input.mousePosition.x, Screen.height - Input.mousePosition.y))) {					
					if (trapeze.tag == "Trapeze" || trapeze.tag == "TrapezeBegin")
						trapeze.GetComponent<Trapeze> ().StartTouchPlayer ();
				}
			}
			
			if (Input.GetMouseButtonDown (0)) {
				if (!b_Key_Pause.rect.Contains (new Vector2 (Input.mousePosition.x, Screen.height - Input.mousePosition.y))) {
					if ((player.STATE == Define.STATE_CHAR_FIRE_BY_CANON || player.STATE == Define.STATE_CHAR_JUMP) && player.stillInCicleTrapeze) {

						if (trapeze.tag == "Trapeze" || trapeze.tag == "TrapezeBegin"){
							trapeze.GetComponent<Trapeze> ().AddPlayer ();

							player.ChangeState (Define.STATE_CHAR_ROTATING_ROPE);

						}else{
							if(trapeze.GetComponent<Ballon> ().timeActiveBoom != 0)
								return;

							trapeze.GetComponent<Ballon> ().AddPlayer ();

							player.ChangeState (Define.STATE_CHAR_ROTATING_BALL);
						}
					}
				}
			}
			
			if (Input.GetMouseButtonUp (0)) {
				if (!b_Key_Pause.rect.Contains (new Vector2 (Input.mousePosition.x, Screen.height - Input.mousePosition.y))) {
					if (trapeze.tag == "Trapeze" || trapeze.tag == "TrapezeBegin") {
						trapeze.GetComponent<Trapeze> ().StopTouchPlayer ();
						trapeze.GetComponent<Trapeze> ().RemovePlayer ();
					}else
						trapeze.GetComponent<Ballon> ().RemovePlayer();
					
					RemoveTrapeze ();
					
					if (player.STATE != Define.STATE_CHAR_JUMP && player.STATE != Define.STATE_CHAR_FIRE_BY_CANON) {

						player.ChangeState (Define.STATE_CHAR_JUMP);
						
						player.transform.eulerAngles = Vector3.zero;

						Camera.main.GetComponent<MainCamera>().stopUpdatePosition();
					}
				}
			}
			
		}
		
		if (b_Key_Pause.isJustClick ()) {
			ChangeState(Define.STATE_SCENE_PAUSE);
		}
	}

	void ActionTouch(){
		foreach (Touch touch in Input.touches) {
			if (trapeze != null && player.STATE != Define.STATE_CHAR_FALL) {
				
				if (touch.phase == TouchPhase.Began){
					if(!b_Key_Pause.rect.Contains(new Vector2(touch.position.x, Screen.height - touch.position.y))){
						if((player.STATE == Define.STATE_CHAR_FIRE_BY_CANON || player.STATE == Define.STATE_CHAR_JUMP) && player.stillInCicleTrapeze){							

							if(trapeze.tag == "Trapeze" || trapeze.tag == "TrapezeBegin"){
								trapeze.GetComponent<Trapeze>().AddPlayer();

								player.ChangeState(Define.STATE_CHAR_ROTATING_ROPE);
							}else{
								trapeze.GetComponent<Ballon>().AddPlayer();

								player.ChangeState(Define.STATE_CHAR_ROTATING_BALL);
							}
							
						}
					}
				}
				
				if(touch.phase == TouchPhase.Ended){		
					if(!b_Key_Pause.rect.Contains(new Vector2(touch.position.x, Screen.height - touch.position.y))){
						if(trapeze.tag == "Trapeze" || trapeze.tag == "TrapezeBegin"){
							trapeze.GetComponent<Trapeze>().StopTouchPlayer();
							trapeze.GetComponent<Trapeze>().RemovePlayer();
						}else
							trapeze.GetComponent<Ballon> ().RemovePlayer();
						
						RemoveTrapeze();
						
						if(player.STATE != Define.STATE_CHAR_JUMP){
							player.ChangeState(Define.STATE_CHAR_JUMP);
							
							player.transform.eulerAngles = Vector3.zero;
						}
					}
				}
				
				if(touch.phase == TouchPhase.Stationary){	
					if(!b_Key_Pause.rect.Contains(new Vector2(touch.position.x, Screen.height - touch.position.y))){
						if(trapeze.tag == "Trapeze" || trapeze.tag == "TrapezeBegin")
							trapeze.GetComponent<Trapeze>().StartTouchPlayer();
					}
				}
			}
			if (b_Key_Pause.isJustClick ()) {
				ChangeState(Define.STATE_SCENE_PAUSE);
			}
		}
	}
	#endregion

	#region ChangeState
	public void ChangeState(int state){
		stageController.stopAllStage ();

		switch (state) {
			
		case Define.STATE_SCENE_PAUSE:			

			stageController.add(new Scene_Stage_Pause());
			break;	

		case Define.STATE_SCENE_RUN:			
			break;	
		}
		
		
		STATE = state;		
	}
	#endregion

	#region Draw
	void OnGUI(){
		b_Key_Pause.Draw ();

		stageController.Draw ();


		GUI.Label (new Rect (0, 0, 50, 50), icon_Coin);

		GUI.skin.label.alignment = TextAnchor.MiddleLeft;
		GUI.skin.label.normal.textColor = Color.black;
		GUI.skin.label.fontSize = 20;
		GUI.Label (new Rect (50, 0, 100, 50), coin.ToString());

		if (showCombo) {
			GUI.skin.label.alignment = TextAnchor.MiddleLeft;
			GUI.skin.label.normal.textColor = Color.black;
			GUI.skin.label.fontSize = 40;
			GUI.Label (new Rect (Screen.width - 300, Screen.height - 150, 200, 50), "+1 Combo");

			if(Time.time - delayTimeShowCombo >= 0.5f)
				showCombo = false;
		}
	}
	#endregion

	public void AddTrapeze(GameObject trapeze){
		this.trapeze = trapeze;
	}

	public void RemoveTrapeze(){
		this.trapeze = null;
	}

	void UpdateCoin(){
		if (coin < player.coin)
			coin += 10;
	}

	public void ShowCombo(){
		showCombo = true;

		delayTimeShowCombo = Time.time;
	}
}
