using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Utils {

	// Create a rectangle from the top left of screen
	public static Rect RectTopLeft(float x, float y, float w, float h)
	{		
		return new Rect(x, y , w , h);
	}
	// Create a rectangle in the center of screen
	public static Rect RectCenter(float w, float h)
	{
		return new Rect((Screen.width - w) / 2, (Screen.height - h) / 2, w, h);
	}

	// Split a rectangle into a list of small rectangles (horizontal)
	public static List<Rect> SplitRectHorizontal(Rect bigRect, int num, float margin)
	{
		List<Rect> rects = new List<Rect>();
		float w = (bigRect.width - (num - 1) * margin) / num;
		for(int i = 0; i < num; i++)
		{
			Rect rect = new Rect(bigRect.x + i * (w + margin), bigRect.y, w, bigRect.height);
			rects.Add(rect);
		}
		
		return rects;
	}

	// Split a rectangle into a list of small rectangles (vertical)
	public static List<Rect> SplitRectVertical(Rect bigRect, int num, float margin)
	{
		List<Rect> rects = new List<Rect>();
		float h = (bigRect.height - (num - 1) * margin) / num;
		for(int i = 0; i < num; i++)
		{
			Rect rect = new Rect(bigRect.x, bigRect.y + i * (h + margin), bigRect.width, h);
			rects.Add(rect);
		}
		
		return rects;
	}

	// Draw text in a rectangle, margin center
	public static void DrawTextCenter(Rect rect, string text, int size, Color color)
	{
		GUIStyle style = new GUIStyle();
		style.alignment = TextAnchor.MiddleCenter;
		style.fontSize = size;	
		style.normal.textColor = color;
		GUI.Label(rect, text, style);
	}

	// Draw text in a rectangle, margin left
	public static void DrawTextLeft(Rect rect, string text, int size, Color color)
	{
		GUIStyle style = new GUIStyle();
		style.alignment = TextAnchor.MiddleLeft;
		style.fontSize = size;	
		style.normal.textColor = color;
		GUI.Label(rect, text, style);
	}

	// Draw text in a rectangle, margin right
	public static void DrawTextRight(Rect rect, string text, int size, Color color)
	{
		GUIStyle style = new GUIStyle();
		style.alignment = TextAnchor.MiddleRight;
		style.fontSize = size;	
		style.normal.textColor = color;
		GUI.Label(rect, text, style);
	}

	// Create a rectangle margin from top and left of a big rectangle
	public static Rect RectTopLeft(Rect bigRect, float x, float y, float w, float h)
	{
		return new Rect(bigRect.x + x, bigRect.y + y, w, h);
	}

	// Create a rectangle margin from top and right of big rectangle
	public static Rect RectTopRight(Rect bigRect, float x, float y, float w, float h)
	{
		return new Rect(bigRect.xMax - x - w, bigRect.y + y, w, h);
	}

	// Create a rectangle margin from bottom and left of big rectangle
	public static Rect RectBottomLeft(Rect bigRect, float x, float y, float w, float h)
	{
		return new Rect(bigRect.x + x, bigRect.yMax - y - h, w, h);
	}

	// Create a rectangle margin from bottom and right of big rectangle
	public static Rect RectBottomRight(Rect bigRect, float x, float y, float w, float h)
	{
		return new Rect (bigRect.xMax - x - w, bigRect.yMax - y - h, w, h);
	}

	// Create a rectangle in center of big rectangle
	public static Rect RectCenter(Rect bigRect, float w, float h)
	{
		return new Rect (bigRect.x + (bigRect.width - w) / 2, bigRect.y + (bigRect.height - h) / 2, w, h);
	}

	// Create a rect margin from top and in center of big rectangle
	public static Rect RectTopCenter(Rect bigRect, float top, float w, float h)
	{
		return new Rect (bigRect.x + (bigRect.width - w) / 2, bigRect.y + top, w, h);
	}

	// Create a rect margin from bottom and in center of big rectangle
	public static Rect RectBottomCenter(Rect bigRect, float bottom, float w, float h)
	{
		return new Rect (bigRect.x + (bigRect.width - w) / 2, bigRect.yMax - bottom - h, w, h);
	}
}
