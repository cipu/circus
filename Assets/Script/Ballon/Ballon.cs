using UnityEngine;
using System.Collections;

public class Ballon : MonoBehaviour {
	public			float 			speed = 1.5f;

	private			bool 			havePlayer;

	public			float 			timeActiveBoom;

	void Start () {
		
	}
	
	void Update () {
		if (timeActiveBoom == 0) {
			if (this.name == "BallonSky")
				transform.Translate (Vector3.up * speed * Time.deltaTime);
			else if (this.name == "BallonBoom") {
//				Vector3 scaleA = new Vector3 (2, 2, 2);			
//				
//				float smooth = 0.5f;
//				transform.localScale = Vector3.Lerp (transform.localScale, scaleA, smooth * Time.deltaTime);
//				
//				if (transform.localScale.x >= 1.8f) {
//					actionBoom ();
//				}
			}
			
		}else if (timeActiveBoom != 0 && Time.time - timeActiveBoom > 0.8f) {
			GameObject.Destroy(gameObject);
		}
	}

	public void AddPlayer(){
		havePlayer = true;

		this.enabled = true;
	}
	
	public void RemovePlayer(){
		havePlayer = false;
	}

	public void actionBoom(){
		timeActiveBoom = Time.time;

		GameObject ballon = transform.FindChild ("balloon_boom").gameObject;

		ballon.GetComponent<SpriteRenderer> ().enabled = false;

		transform.FindChild ("boom").GetComponent<SpriteRenderer>().enabled = true;

		if(havePlayer)
			GameObject.FindWithTag ("Player").GetComponent<Player> ().ChangeState (Define.STATE_CHAR_FALL);
	}
}
