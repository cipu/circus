﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour {
	public			GameObject 				target;
	private			Player					player; 

	public			GameObject 				backGround;


	public			bool					canMoveUpdatePosition;					
	public			bool					moveLeft;

	private			int					speedMoveCamera;

	void Start () {
		player = target.GetComponent<Player> ();


		speedMoveCamera = 5;
	}
	

	void Update () {
		if (player.STATE == Define.STATE_CHAR_FALL)
			return;

		if (player.STATE == Define.STATE_CHAR_FIRE_BY_CANON) {
			transform.position = new Vector3 (target.transform.position.x, target.transform.position.y, -10);
		}else if (player.STATE == Define.STATE_CHAR_IN_CANON) {
			stopUpdatePosition();

			if(player.trapeze.GetComponentInChildren<Canon>().whichDirectionFace == 45){
				transform.position = Vector3.Lerp(transform.position, (player.trapeze.transform.position + new Vector3(7.5f, 3.5F, 0)), 2f * Time.deltaTime);
				transform.position = new Vector3 (transform.position.x, transform.position.y, -10);
			}else if(player.trapeze.GetComponentInChildren<Canon>().whichDirectionFace == 90){
				transform.position = Vector3.Lerp(transform.position, (player.trapeze.transform.position + new Vector3(0,3.5F,0)), 2f * Time.deltaTime);
				transform.position = new Vector3 (transform.position.x, transform.position.y, -10);
			}else if(player.trapeze.GetComponentInChildren<Canon>().whichDirectionFace == 270){
				transform.position = Vector3.Lerp(transform.position, (player.trapeze.transform.position + new Vector3(0,-4.5f,0)), 2f * Time.deltaTime);
				transform.position = new Vector3 (transform.position.x, transform.position.y, -10);
			}else if(player.trapeze.GetComponentInChildren<Canon>().whichDirectionFace == 315){
				transform.position = Vector3.Lerp(transform.position, (player.trapeze.transform.position + new Vector3(7.5f,-4.5f,0)), 2f * Time.deltaTime);
				transform.position = new Vector3 (transform.position.x, transform.position.y, -10);
			}


		}else if (player.STATE == Define.STATE_CHAR_JUMP){
			
			transform.position = Vector3.Lerp(transform.position, target.transform.position, 0.8f * Time.deltaTime);
			transform.position = new Vector3 (transform.position.x, transform.position.y, -10);
		}



		if (canMoveUpdatePosition) {
			if(moveLeft){
				transform.Translate(Vector3.left * speedMoveCamera * Time.deltaTime);

				if( transform.position.x - player.transform.position.x <= 5){
					transform.position = new Vector3 (player.transform.position.x - 5, transform.position.y, -10);


					stopUpdatePosition();
				}
			}else{
				transform.Translate(Vector3.right * speedMoveCamera * Time.deltaTime);

				if( transform.position.x - player.transform.position.x >= 5){
					transform.position = new Vector3 (player.transform.position.x + 5, transform.position.y, -10);

					stopUpdatePosition();
				}
			}
		}

		backGround.transform.position = new Vector3(transform.position.x, transform.position.y, 5);
	}

	public void StartUpdatePosition () {
		canMoveUpdatePosition = true;

		if (transform.position.x - player.transform.position.x > 5)
			moveLeft = true;
		else if(transform.position.x - player.transform.position.x < 5)
			moveLeft = false;
	}

	public void stopUpdatePosition(){
		canMoveUpdatePosition = false;
	}
}
