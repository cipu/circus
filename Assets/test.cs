﻿using UnityEngine;
using System.Collections;

public class test : MonoBehaviour {
	Vector2 startPos;
	float power = 590.0f;
	float interval = 1/30.0f; 
	GameObject[] ind;
	bool shot, aiming;
	int dots = 50;
	public GameObject Dot;

	void Start (){
		transform.rigidbody2D.isKinematic = true;
//		ind = new GameObject[dots];
//		for(int i = 0; i<dots; i++){
//			GameObject dot = (GameObject)Instantiate(Dot);
//			dot.renderer.enabled = false;
//			ind[i] = dot;
//		}
	}
	
	void Update (){
//		if(shot) return;
//		if(Input.GetAxis("Fire1") == 1){
//			if(!aiming){
//				aiming = true;
//				startPos = Input.mousePosition;
////				ShowPath();
//			}
//			else{
//				CalculatePath();
//			}
//			
//		}
//		else if(aiming && !shot){
		if(Input.GetMouseButtonDown(0)){
			transform.rigidbody2D.isKinematic =  false; 
			transform.rigidbody2D.AddForce(GetForce(Input.mousePosition));        
			shot = true;
			aiming = false;
//			HidePath();
		}
		transform.Rotate (Vector3.forward * 50 * Time.deltaTime);
	}
	
	Vector2 GetForce(Vector3 mouse){
		Vector2 v1 = new Vector2 (startPos.x, startPos.y);
		
		Vector2 v2 = GetVector(270);

		Vector2 vSum = (v1 + v2);

		print("v1: " + v1 +" + "+ "v2: " + v2 + "v1 - v2 = " + vSum.normalized);

		return vSum.normalized * power;
	}
	
//	void CalculatePath(){
//		ind[0].transform.position = transform.position; //set frist dot to ball position
//		Vector2 vel = GetForce(Input.mousePosition); //get velocity
//		
//		for(int i = 1; i < dots; i++){          
//			ind[i].renderer.enabled = true; //make them visible
//			Vector3 point = PathPoint(transform.position, vel, i); //get position of the dot 
//			point.z = -1.0f;
//			ind[i].transform.position = point;
//		} 
//	}
//	void OnDrawGizmos()
//	{
//		if (G == Vector3.zero)
//		{
//			// a hacky way of making sure this gets initialized in editor too...
//			// this assumes 60 samples / sec
//			G = new Vector3(0,-9.8f,0) / 360f;
//		}
//		Vector3 momentum = StartVelocity;
//		Vector3 pos = gameObject.transform.position;
//		Vector3 last = gameObject.transform.position;
//		for (int i = 0; i < (int) (PredictionTime * 60); i++)
//		{
//			momentum += G;
//			pos += momentum;
//			Gizmos.DrawLine(last, pos);
//			last = pos;
//		}
//		
//	}
//	Vector2 PathPoint(Vector2 startP, Vector2 startVel, int n){
//		//Standard formula for trajectory prediction
//		float t = interval;
//		Vector2 stepVelocity = t*startVel;
//		Vector2 StepGravity = t*t*Physics.gravity;
//		
//		Vector2 whattoreturn = ((startP + (n * stepVelocity)+(n*n+n)*StepGravity) * 0.5f);
//		
//		return whattoreturn;
//	}
//
//
//	public Vector3 StartVelocity;
//	public float PredictionTime;
//	private Vector3 G;
//	
//

	//	Vector2 GetForce(Vector3 mouse){
//			Vector2 v1 = new Vector2 (startPos.x, startPos.y);
//	
//			Vector2 v2 = GetVector(90);
//	
//			print("v1: " + v1 +" + "+ "v2: " + v2);
//	
//			return (v1 + v2) * power;		
	//	}
	//
	Vector2 GetVector(float angle){
		float a = Mathf.Cos(angle* Mathf.Deg2Rad) * 10;
		float b = Mathf.Sin (angle* Mathf.Deg2Rad) * 10;

		return new Vector2 (a, b);
	}
	
}
